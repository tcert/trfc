TRFC - TCERT RFC

Official projects
=================

Number     Description - Project URL
TRFC1      TCERT RFC Registry - https://bitbucket.org/tcert/trfc
TRFC2      TCQA: TCERT Qualified Auditors certification - https://bitbucket.org/tcert/tcqa

Pending projects
================

TCNE	Telecom Core Network Element security guidelines
